﻿using System;
using System.Drawing;

namespace IntegralEquations
{
    public static class Helpers
    {
        public static nzy3D.Colors.Color ToNzyColor(this Color c)
        {
            return new nzy3D.Colors.Color(Convert.ToDouble(c.R / 255), Convert.ToDouble(c.G / 255.0), Convert.ToDouble(c.B / 255.0));
        }



    }
}
