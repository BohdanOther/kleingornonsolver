﻿using IntegralEquations.Solver;

namespace IntegralEquations.Views
{
    public interface IProblemView
    {
        KleinGordonProblem Problem { get; }
    }
}
