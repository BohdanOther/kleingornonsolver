﻿using IntegralEquations.Math.Functions;
using IntegralEquations.Solver;
using IntegralEquations.Solver.Core;
using System;
using System.Windows.Forms;
using IntegralEquations.Math;

namespace IntegralEquations.Views
{
    public partial class ProblemView : UserControl, IProblemView
    {
        public KleinGordonProblem Problem => DesignMode ? null : GetProblem();

        public ProblemView()
        {
            InitializeComponent();

            this.Dock = DockStyle.Fill;
        }

        private KleinGordonProblem GetProblem()
        {
            var rawGamma1 = tbGamma1.Text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var gamma11 = new SymbolicScalarFunction(rawGamma1[0], 1, "t");
            var gamma12 = new SymbolicScalarFunction(rawGamma1[1], 1, "t");

            var gamma1 = new ParametricFunction(gamma11, gamma12);

            //try
            //{
            //    gamma1.Val((1.0).ToVectorArg());
            //}
            //catch (Exception)
            //{
            //    throw new Exception("Wrong gamma 1 input");
            //}

            var rawGamma2 = tbGamma2.Text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var gamma21 = new SymbolicScalarFunction(rawGamma2[0], 1, "t");
            var gamma22 = new SymbolicScalarFunction(rawGamma2[1], 1, "t");

            var gamma2 = new ParametricFunction(gamma21, gamma22);

            //try
            //{
            //    gamma2.Val((1.0).ToVectorArg());
            //}
            //catch (Exception)
            //{
            //    throw new Exception("Wrong gamma 2 input");
            //}

            var f1 = new SymbolicScalarFunction(tbF1.Text, 2);
     

            var f2 = new SymbolicScalarFunction(tbF2.Text, 2);


            var lambda = new SymbolicScalarFunction(tbLambda.Text, 2);


            var kappa = 0.0;
        
         
                kappa = double.Parse(tbKappa.Text);
   

            return new KleinGordonProblem
            {
                F1 = f1,
                F2 = f2,
                OuterBound = new ParametricBoundary(gamma1),
                InnerBound = new ParametricBoundary(gamma2),
                Lambda = lambda,
                Kappa = kappa,
            };
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            tbGamma1.Text = "2*Cos(t), 2*Sin(t)";
            tbGamma2.Text = "0.5*Cos(t), 0.5*Sin(t)";
            tbLambda.Text = "1.0";
            tbKappa.Text = "1.0";
            tbF1.Text = "x1 + x2";
            tbF2.Text = "x1 - x2";
        }
    }
}
