﻿namespace IntegralEquations.Views
{
    partial class ProblemView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelGamma1 = new System.Windows.Forms.Label();
            this.labelKappa = new System.Windows.Forms.Label();
            this.labelLambda = new System.Windows.Forms.Label();
            this.labelF2 = new System.Windows.Forms.Label();
            this.labelF1 = new System.Windows.Forms.Label();
            this.labelGamma2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbKappa = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbLambda = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbF2 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbF1 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbGamma2 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbGamma1 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.btnDefault = new IntegralEquations.UI.Controls.DarkButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelGamma1
            // 
            this.labelGamma1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGamma1.AutoSize = true;
            this.labelGamma1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGamma1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelGamma1.Location = new System.Drawing.Point(3, 6);
            this.labelGamma1.Name = "labelGamma1";
            this.labelGamma1.Size = new System.Drawing.Size(74, 15);
            this.labelGamma1.TabIndex = 0;
            this.labelGamma1.Text = "Gamma1";
            // 
            // labelKappa
            // 
            this.labelKappa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelKappa.AutoSize = true;
            this.labelKappa.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKappa.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelKappa.Location = new System.Drawing.Point(3, 146);
            this.labelKappa.Name = "labelKappa";
            this.labelKappa.Size = new System.Drawing.Size(74, 15);
            this.labelKappa.TabIndex = 5;
            this.labelKappa.Text = "Kappa";
            // 
            // labelLambda
            // 
            this.labelLambda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLambda.AutoSize = true;
            this.labelLambda.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLambda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelLambda.Location = new System.Drawing.Point(3, 118);
            this.labelLambda.Name = "labelLambda";
            this.labelLambda.Size = new System.Drawing.Size(74, 15);
            this.labelLambda.TabIndex = 4;
            this.labelLambda.Text = "Lambda";
            // 
            // labelF2
            // 
            this.labelF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelF2.AutoSize = true;
            this.labelF2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelF2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelF2.Location = new System.Drawing.Point(3, 90);
            this.labelF2.Name = "labelF2";
            this.labelF2.Size = new System.Drawing.Size(74, 15);
            this.labelF2.TabIndex = 3;
            this.labelF2.Text = "f2";
            // 
            // labelF1
            // 
            this.labelF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelF1.AutoSize = true;
            this.labelF1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelF1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelF1.Location = new System.Drawing.Point(3, 62);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(74, 15);
            this.labelF1.TabIndex = 2;
            this.labelF1.Text = "f1";
            // 
            // labelGamma2
            // 
            this.labelGamma2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGamma2.AutoSize = true;
            this.labelGamma2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGamma2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelGamma2.Location = new System.Drawing.Point(3, 34);
            this.labelGamma2.Name = "labelGamma2";
            this.labelGamma2.Size = new System.Drawing.Size(74, 15);
            this.labelGamma2.TabIndex = 1;
            this.labelGamma2.Text = "Gamma2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.37255F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.62745F));
            this.tableLayoutPanel1.Controls.Add(this.tbKappa, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbLambda, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbF2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbF1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbGamma2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelGamma2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelF1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelF2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelLambda, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelKappa, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelGamma1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbGamma1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDefault, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(255, 400);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tbKappa
            // 
            this.tbKappa.BackColor = System.Drawing.Color.White;
            this.tbKappa.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbKappa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbKappa.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKappa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbKappa.Location = new System.Drawing.Point(83, 143);
            this.tbKappa.Name = "tbKappa";
            this.tbKappa.Size = new System.Drawing.Size(169, 22);
            this.tbKappa.TabIndex = 11;
            this.tbKappa.Text = "real number";
            this.tbKappa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbLambda
            // 
            this.tbLambda.BackColor = System.Drawing.Color.White;
            this.tbLambda.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbLambda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLambda.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLambda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbLambda.Location = new System.Drawing.Point(83, 115);
            this.tbLambda.Name = "tbLambda";
            this.tbLambda.Size = new System.Drawing.Size(169, 22);
            this.tbLambda.TabIndex = 10;
            this.tbLambda.Text = "f (x1, x2)";
            this.tbLambda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbF2
            // 
            this.tbF2.BackColor = System.Drawing.Color.White;
            this.tbF2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbF2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbF2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbF2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbF2.Location = new System.Drawing.Point(83, 87);
            this.tbF2.Name = "tbF2";
            this.tbF2.Size = new System.Drawing.Size(169, 22);
            this.tbF2.TabIndex = 9;
            this.tbF2.Text = "f ( x1, x2 )";
            this.tbF2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbF1
            // 
            this.tbF1.BackColor = System.Drawing.Color.White;
            this.tbF1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbF1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbF1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbF1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbF1.Location = new System.Drawing.Point(83, 59);
            this.tbF1.Name = "tbF1";
            this.tbF1.Size = new System.Drawing.Size(169, 22);
            this.tbF1.TabIndex = 8;
            this.tbF1.Text = "f ( x1, x2 )";
            this.tbF1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbGamma2
            // 
            this.tbGamma2.BackColor = System.Drawing.Color.White;
            this.tbGamma2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbGamma2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbGamma2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGamma2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbGamma2.Location = new System.Drawing.Point(83, 31);
            this.tbGamma2.Name = "tbGamma2";
            this.tbGamma2.Size = new System.Drawing.Size(169, 22);
            this.tbGamma2.TabIndex = 7;
            this.tbGamma2.Text = "F ( x(t), y(t) )";
            this.tbGamma2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbGamma1
            // 
            this.tbGamma1.BackColor = System.Drawing.Color.White;
            this.tbGamma1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbGamma1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbGamma1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGamma1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbGamma1.Location = new System.Drawing.Point(83, 3);
            this.tbGamma1.Name = "tbGamma1";
            this.tbGamma1.Size = new System.Drawing.Size(169, 22);
            this.tbGamma1.TabIndex = 6;
            this.tbGamma1.Text = "F ( x(t), y(t) )";
            this.tbGamma1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btnDefault
            // 
            this.btnDefault.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDefault.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefault.Location = new System.Drawing.Point(83, 171);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(169, 23);
            this.btnDefault.TabIndex = 15;
            this.btnDefault.Text = "defaults";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // ProblemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ProblemView";
            this.Size = new System.Drawing.Size(255, 400);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelGamma1;
        private System.Windows.Forms.Label labelKappa;
        private System.Windows.Forms.Label labelLambda;
        private System.Windows.Forms.Label labelF2;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Label labelGamma2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private UI.Controls.DarkTextBox tbKappa;
        private UI.Controls.DarkTextBox tbLambda;
        private UI.Controls.DarkTextBox tbF2;
        private UI.Controls.DarkTextBox tbF1;
        private UI.Controls.DarkTextBox tbGamma2;
        private UI.Controls.DarkTextBox tbGamma1;
        private UI.Controls.DarkButton btnDefault;
    }
}
