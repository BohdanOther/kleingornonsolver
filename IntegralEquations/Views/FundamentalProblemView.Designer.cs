﻿namespace IntegralEquations.Views
{
    partial class FundamentalProblemView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelGamma1 = new System.Windows.Forms.Label();
            this.labelKappa = new System.Windows.Forms.Label();
            this.labelLambda = new System.Windows.Forms.Label();
            this.labelGamma2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbKappa = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbLambda = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbGamma2 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.tbGamma1 = new IntegralEquations.UI.Controls.DarkTextBox();
            this.labelY = new System.Windows.Forms.Label();
            this.tbY = new IntegralEquations.UI.Controls.DarkTextBox();
            this.btnDefault = new IntegralEquations.UI.Controls.DarkButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelGamma1
            // 
            this.labelGamma1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGamma1.AutoSize = true;
            this.labelGamma1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGamma1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelGamma1.Location = new System.Drawing.Point(3, 6);
            this.labelGamma1.Name = "labelGamma1";
            this.labelGamma1.Size = new System.Drawing.Size(69, 15);
            this.labelGamma1.TabIndex = 0;
            this.labelGamma1.Text = "Gamma1";
            // 
            // labelKappa
            // 
            this.labelKappa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelKappa.AutoSize = true;
            this.labelKappa.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKappa.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelKappa.Location = new System.Drawing.Point(3, 90);
            this.labelKappa.Name = "labelKappa";
            this.labelKappa.Size = new System.Drawing.Size(69, 15);
            this.labelKappa.TabIndex = 5;
            this.labelKappa.Text = "Kappa";
            // 
            // labelLambda
            // 
            this.labelLambda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLambda.AutoSize = true;
            this.labelLambda.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLambda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelLambda.Location = new System.Drawing.Point(3, 62);
            this.labelLambda.Name = "labelLambda";
            this.labelLambda.Size = new System.Drawing.Size(69, 15);
            this.labelLambda.TabIndex = 4;
            this.labelLambda.Text = "Lambda";
            // 
            // labelGamma2
            // 
            this.labelGamma2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGamma2.AutoSize = true;
            this.labelGamma2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGamma2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelGamma2.Location = new System.Drawing.Point(3, 34);
            this.labelGamma2.Name = "labelGamma2";
            this.labelGamma2.Size = new System.Drawing.Size(69, 15);
            this.labelGamma2.TabIndex = 1;
            this.labelGamma2.Text = "Gamma2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.37255F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.62745F));
            this.tableLayoutPanel1.Controls.Add(this.tbKappa, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbLambda, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbGamma2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelGamma2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelLambda, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelKappa, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelGamma1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbGamma1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelY, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbY, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnDefault, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(242, 334);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tbKappa
            // 
            this.tbKappa.BackColor = System.Drawing.Color.White;
            this.tbKappa.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbKappa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbKappa.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKappa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbKappa.Location = new System.Drawing.Point(78, 87);
            this.tbKappa.Name = "tbKappa";
            this.tbKappa.Size = new System.Drawing.Size(161, 22);
            this.tbKappa.TabIndex = 11;
            this.tbKappa.Text = "real number";
            this.tbKappa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbLambda
            // 
            this.tbLambda.BackColor = System.Drawing.Color.White;
            this.tbLambda.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbLambda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLambda.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLambda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbLambda.Location = new System.Drawing.Point(78, 59);
            this.tbLambda.Name = "tbLambda";
            this.tbLambda.Size = new System.Drawing.Size(161, 22);
            this.tbLambda.TabIndex = 10;
            this.tbLambda.Text = "f (x, y)";
            this.tbLambda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbGamma2
            // 
            this.tbGamma2.BackColor = System.Drawing.Color.White;
            this.tbGamma2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbGamma2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbGamma2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGamma2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbGamma2.Location = new System.Drawing.Point(78, 31);
            this.tbGamma2.Name = "tbGamma2";
            this.tbGamma2.Size = new System.Drawing.Size(161, 22);
            this.tbGamma2.TabIndex = 7;
            this.tbGamma2.Text = "F ( x(t), y(t) )";
            this.tbGamma2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbGamma1
            // 
            this.tbGamma1.BackColor = System.Drawing.Color.White;
            this.tbGamma1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbGamma1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbGamma1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGamma1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbGamma1.Location = new System.Drawing.Point(78, 3);
            this.tbGamma1.Name = "tbGamma1";
            this.tbGamma1.Size = new System.Drawing.Size(161, 22);
            this.tbGamma1.TabIndex = 6;
            this.tbGamma1.Text = "F ( x(t), y(t) )";
            this.tbGamma1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // labelY
            // 
            this.labelY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelY.AutoSize = true;
            this.labelY.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelY.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelY.Location = new System.Drawing.Point(3, 118);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(69, 15);
            this.labelY.TabIndex = 12;
            this.labelY.Text = "y";
            // 
            // tbY
            // 
            this.tbY.BackColor = System.Drawing.Color.White;
            this.tbY.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbY.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbY.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbY.Location = new System.Drawing.Point(78, 115);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(161, 22);
            this.tbY.TabIndex = 13;
            this.tbY.Text = "y1, y2";
            this.tbY.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btnDefault
            // 
            this.btnDefault.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDefault.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefault.Location = new System.Drawing.Point(78, 143);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(161, 23);
            this.btnDefault.TabIndex = 14;
            this.btnDefault.Text = "defaults";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // FundamentalProblemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FundamentalProblemView";
            this.Size = new System.Drawing.Size(242, 334);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UI.Controls.DarkTextBox tbGamma1;
        private System.Windows.Forms.Label labelGamma1;
        private System.Windows.Forms.Label labelKappa;
        private System.Windows.Forms.Label labelLambda;
        private System.Windows.Forms.Label labelGamma2;
        private UI.Controls.DarkTextBox tbGamma2;
        private UI.Controls.DarkTextBox tbLambda;
        private UI.Controls.DarkTextBox tbKappa;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelY;
        private UI.Controls.DarkTextBox tbY;
        private UI.Controls.DarkButton btnDefault;
    }
}
