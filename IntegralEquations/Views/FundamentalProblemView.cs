﻿using IntegralEquations.Math.Functions;
using IntegralEquations.Solver;
using IntegralEquations.Solver.Core;
using System;
using System.Windows.Forms;
using IntegralEquations.Math;
using IntegralEquations.Solver.FundamentalSolution;

namespace IntegralEquations.Views
{
    public partial class FundamentalProblemView : UserControl, IProblemView
    {
        public KleinGordonProblem Problem => DesignMode ? null : GetProblem();

        public FundamentalProblemView()
        {
            InitializeComponent();

            this.Dock = DockStyle.Fill;
        }

        private KleinGordonProblem GetProblem()
        {
            var rawGamma1 = tbGamma1.Text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var gamma11 = new SymbolicScalarFunction(rawGamma1[0], 1, "t");
            var gamma12 = new SymbolicScalarFunction(rawGamma1[1], 1, "t");

            var gamma1 = new ParametricFunction(gamma11, gamma12);

            try
            {
                gamma1.Val((1.0).ToVectorArg());
            }
            catch (Exception)
            {
                throw new Exception("Wrong gamma 1 input");
            }

            var rawGamma2 = tbGamma2.Text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var gamma21 = new SymbolicScalarFunction(rawGamma2[0], 1, "t");
            var gamma22 = new SymbolicScalarFunction(rawGamma2[1], 1, "t");

            var gamma2 = new ParametricFunction(gamma21, gamma22);

            try
            {
                gamma2.Val((1.0).ToVectorArg());
            }
            catch (Exception)
            {
                throw new Exception("Wrong gamma 2 input");
            }

            var lambda = new SymbolicScalarFunction(tbLambda.Text, 2);
            try
            {
                lambda.Val((1.0).ToVectorArg());
            }
            catch (Exception)
            {
                throw new Exception("Wrong lambda input");
            }

            var kappa = 0.0;
            try
            {
                kappa = double.Parse(tbKappa.Text);
            }
            catch (Exception)
            {
                throw new Exception("Wrong kappa input");
            }

            var y = new double[2];
            try
            {
                var data = tbY.Text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var y1 = double.Parse(data[0]);
                var y2 = double.Parse(data[1]);

                y[0] = y1;
                y[1] = y2;
            }
            catch (Exception)
            {
                throw new Exception("Wrong y input");

            }

            return new FundamentalProblem(y)
            {
                OuterBound = new ParametricBoundary(gamma1),
                InnerBound = new ParametricBoundary(gamma2),
                Lambda = lambda,
                Kappa = kappa,
            };
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            tbGamma1.Text = "2*Cos(t), 2*Sin(t)";
            tbGamma2.Text = "0.5*Cos(t), 0.5*Sin(t)";
            tbLambda.Text = "1.0";
            tbKappa.Text = "1.0";
            tbY.Text = "0.0, 0.0";
        }
    }
}
