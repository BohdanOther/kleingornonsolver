﻿
using IntegralEquations.Math.Functions;
using nzy3D.Plot3D.Builder;
using System;

namespace IntegralEquations.Plot3D
{
    class FunctionMapper : Mapper
    {
        private Func<double, double, double> _func;

        public FunctionMapper(IScalarFunction f)
        {
            _func = (x, y) => f.Val(new[] { x, y });
        }

        public FunctionMapper(Func<double[], double> func)
        {

            _func = (x, y) => func(new[] { x, y });
        }

        public FunctionMapper(Func<double, double, double> func)
        {
            _func = func;
        }

        public override double f(double x, double y)
        {
            return _func(x, y);
        }
    }
}

