﻿namespace IntegralEquations
{
    partial class MainWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.renderer = new nzy3D.Plot3D.Rendering.View.Renderer3D();
            this.darkTabControl1 = new IntegralEquations.UI.Controls.DarkTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSolve = new IntegralEquations.UI.Controls.DarkButton();
            this.panelProblemView = new System.Windows.Forms.Panel();
            this.problemView1 = new IntegralEquations.Views.ProblemView();
            this.chbUseFundamentalProblem = new IntegralEquations.UI.Controls.DarkCheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.err = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.darkTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panelProblemView.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.darkTabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.splitContainer1.Panel2.Controls.Add(this.renderer);
            this.splitContainer1.Size = new System.Drawing.Size(779, 469);
            this.splitContainer1.SplitterDistance = 258;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // renderer
            // 
            this.renderer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.renderer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.renderer.Location = new System.Drawing.Point(0, 0);
            this.renderer.Name = "renderer";
            this.renderer.Size = new System.Drawing.Size(520, 469);
            this.renderer.TabIndex = 0;
            this.renderer.VSync = false;
            // 
            // darkTabControl1
            // 
            this.darkTabControl1.Controls.Add(this.tabPage1);
            this.darkTabControl1.Controls.Add(this.tabPage2);
            this.darkTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.darkTabControl1.Location = new System.Drawing.Point(0, 0);
            this.darkTabControl1.Name = "darkTabControl1";
            this.darkTabControl1.SelectedIndex = 0;
            this.darkTabControl1.Size = new System.Drawing.Size(258, 469);
            this.darkTabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.tabPage1.Controls.Add(this.btnSolve);
            this.tabPage1.Controls.Add(this.panelProblemView);
            this.tabPage1.Controls.Add(this.chbUseFundamentalProblem);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(250, 440);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Problem";
            // 
            // btnSolve
            // 
            this.btnSolve.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSolve.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolve.Location = new System.Drawing.Point(8, 409);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(236, 23);
            this.btnSolve.TabIndex = 2;
            this.btnSolve.Text = "Solve";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.btnSolve_Click);
            // 
            // panelProblemView
            // 
            this.panelProblemView.Controls.Add(this.problemView1);
            this.panelProblemView.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelProblemView.Location = new System.Drawing.Point(3, 3);
            this.panelProblemView.Name = "panelProblemView";
            this.panelProblemView.Size = new System.Drawing.Size(244, 292);
            this.panelProblemView.TabIndex = 1;
            // 
            // problemView1
            // 
            this.problemView1.BackColor = System.Drawing.Color.DimGray;
            this.problemView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.problemView1.Location = new System.Drawing.Point(0, 0);
            this.problemView1.Name = "problemView1";
            this.problemView1.Size = new System.Drawing.Size(244, 292);
            this.problemView1.TabIndex = 0;
            // 
            // chbUseFundamentalProblem
            // 
            this.chbUseFundamentalProblem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.chbUseFundamentalProblem.Checked = false;
            this.chbUseFundamentalProblem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbUseFundamentalProblem.Location = new System.Drawing.Point(8, 301);
            this.chbUseFundamentalProblem.Name = "chbUseFundamentalProblem";
            this.chbUseFundamentalProblem.Size = new System.Drawing.Size(223, 23);
            this.chbUseFundamentalProblem.TabIndex = 0;
            this.chbUseFundamentalProblem.Text = "Use \'The fundamental\' problem";
            this.chbUseFundamentalProblem.UseVisualStyleBackColor = false;
            this.chbUseFundamentalProblem.Click += new System.EventHandler(this.chbUseFundamentalProblem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DimGray;
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(250, 440);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Solution";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(89, 413);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(155, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 411);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.n,
            this.err});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(244, 404);
            this.dataGridView1.TabIndex = 0;
            // 
            // n
            // 
            this.n.FillWeight = 60.9137F;
            this.n.HeaderText = "n";
            this.n.Name = "n";
            this.n.ReadOnly = true;
            // 
            // err
            // 
            this.err.FillWeight = 113.0288F;
            this.err.HeaderText = "err";
            this.err.Name = "err";
            this.err.ReadOnly = true;
            // 
            // MainWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 469);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainWindowForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindowForm_FormClosing);
            this.Load += new System.EventHandler(this.MainWindowForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.darkTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panelProblemView.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private UI.Controls.DarkTabControl darkTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private nzy3D.Plot3D.Rendering.View.Renderer3D renderer;
        private System.Windows.Forms.Panel panelProblemView;
        private UI.Controls.DarkCheckBox chbUseFundamentalProblem;
        private Views.ProblemView problemView1;
        private UI.Controls.DarkButton btnSolve;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn n;
        private System.Windows.Forms.DataGridViewTextBoxColumn err;
    }
}

