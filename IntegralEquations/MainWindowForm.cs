﻿using IntegralEquations.Math.Functions;
using IntegralEquations.Plot3D;
using IntegralEquations.Solver;
using IntegralEquations.Solver.Core;
using IntegralEquations.Solver.FundamentalSolution;
using IntegralEquations.UI.Controls;
using IntegralEquations.Views;
using nzy3D.Chart;
using nzy3D.Chart.Controllers.Thread.Camera;
using nzy3D.Colors;
using nzy3D.Colors.ColorMaps;
using nzy3D.Plot3D.Builder;
using nzy3D.Plot3D.Builder.Concrete;
using nzy3D.Plot3D.Primitives;
using nzy3D.Plot3D.Primitives.Axes.Layout;
using nzy3D.Plot3D.Rendering.Canvas;
using System;
using System.Linq;
using System.Windows.Forms;
using static System.Math;

namespace IntegralEquations
{
    public partial class MainWindowForm : Form
    {
        private CameraThreadController controller;
        private IAxeLayout axeLayout;

        Chart chart;

        private IProblemView problemView;

        KleinGornonSolver solver;
        KleinGordonSolution solution;
        KleinGordonProblem problem;
        FundamentalSolution exact;

        private Shape surface;
        public MainWindowForm()
        {
            Math.UseExamples.Test();
            InitializeComponent();

            ToolStripManager.Renderer = new DarkToolStripRenderer();

            solver = new KleinGornonSolver();
        }

        private void InitRenderer(IScalarFunction f)
        {
            // Create a range for the graph generation
            var range = new nzy3D.Maths.Range(-2, 2);
            var steps = 50;

            // Build a nice surface to display with cool alpha colors 
            // (alpha 0.8 for surface color and 0.5 for wireframe)
            // solution here
            surface = Builder.buildRing(new OrthonormalGrid(range, steps, range, steps), new FunctionMapper(f), 0.5f, 2);

            surface.ColorMapper = new ColorMapper(new ColorMapRainbow(), surface.Bounds.zmin, surface.Bounds.zmax, new Color(1, 1, 1, 0.8));
            surface.FaceDisplayed = true;
            surface.WireframeDisplayed = true;
            surface.WireframeColor = Color.CYAN;
            surface.WireframeColor.mul(new Color(1, 1, 1, 0.5));
            // Create the chart and embed the surface within

            chart = new Chart(renderer, Quality.Nicest);
            chart.Scene.Graph.Add(surface);
            axeLayout = chart.AxeLayout;
            axeLayout.MainColor = Color.WHITE;
            chart.View.BackgroundColor = Color.GRAY;

            // Create a mouse control
            var mouse = new nzy3D.Chart.Controllers.Mouse.Camera.CameraMouseController();
            mouse.addControllerEventListener(renderer);
            chart.addController(mouse);

            // This is just to ensure code is reentrant (used when code is not called in Form_Load but another reentrant event)
            DisposeBackgroundThread();

            // Create a thread to control the camera based on mouse movements
            controller = new CameraThreadController();
            controller.addControllerEventListener(renderer);
            mouse.addSlaveThreadController(controller);
            chart.addController(controller);
            controller.Start();
            // Associate the chart with current control
            renderer.setView(chart.View);

            this.Refresh();
        }

        private void DisposeBackgroundThread()
        {
            controller?.Dispose();
        }

        private void MainWindowForm_Load(object sender, EventArgs e)
        {
            if (chbUseFundamentalProblem.Checked)
                problemView = new FundamentalProblemView();
            else
                problemView = new ProblemView();

            //  InitRenderer(new ScalarFunctionHandle(x => FunctionHelpers.Test(x[0], x[1])));
        }

        private void MainWindowForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DisposeBackgroundThread();
        }

        private void SwitchProblemPanels()
        {
            panelProblemView.SuspendLayout();

            panelProblemView.Controls.Clear();
            if (chbUseFundamentalProblem.Checked)
            {
                var c = new FundamentalProblemView();
                problemView = c;
                panelProblemView.Controls.Add(c);
            }
            else
            {
                var c = new ProblemView();
                problemView = c;
                panelProblemView.Controls.Add(c);
            }

            panelProblemView.ResumeLayout(true);

        }

        private void chbUseFundamentalProblem_Click(object sender, EventArgs e)
        {
            SwitchProblemPanels();
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            //// super-ellipse , squircle
            //var gamma2 = new ParametricFunction(
            // new ScalarFunctionHandle(t => 0.5 * Sqrt(Abs(Cos(t[0]))) * Sign(Cos(t[0])), 1),
            //     new ScalarFunctionHandle(t => 1 * Sqrt(Abs(Sin(t[0]))) * Sign(Sin(t[0])), 1)
            // );

            //// Epitrochoid
            //var gamma1 = new ParametricFunction( // super-ellipse , squircle
            //    new ScalarFunctionHandle(t => 6 * Cos(t[0]) - 0.5 * Cos(5 * t[0]), 1),
            //        new ScalarFunctionHandle(t => 6 * Sin(t[0]) - 0.5 * Sin(4 * t[0]), 1)
            //    );

            // var gamma1 = new ParametricFunction(
            //new ScalarFunctionHandle(t => 8 * Sqrt(Abs(Cos(t[0]))) * Sign(Cos(t[0])), 1),
            //    new ScalarFunctionHandle(t => 4 * Sqrt(Abs(Sin(t[0]))) * Sign(Sin(t[0])), 1)
            //);

            var gamma1 = new ParametricBoundary(
                new ParametricFunction(
                    new SymbolicScalarFunction("2*Cos(t)", 1, "t"),
                    new SymbolicScalarFunction("2*Sin(t)", 1, "t")),

                  new ParametricFunction(
                    new SymbolicScalarFunction("-2*Sin(t)", 1, "t"),
                    new SymbolicScalarFunction("2*Cos(t)", 1, "t"))
                );

            var gamma2 = new ParametricBoundary(
                new ParametricFunction(
                    new SymbolicScalarFunction("0.5*Cos(t)", 1, "t"),
                    new SymbolicScalarFunction("0.5*Sin(t)", 1, "t")),

                new ParametricFunction(
                    new SymbolicScalarFunction("-0.5*Sin(t)", 1, "t"),
                    new SymbolicScalarFunction("0.5*Cos(t)", 1, "t"))
                    );

            var lambda = new ScalarFunctionHandle(x => 1, 2);
            var k = 1;
            var y = new[] { 0.0, 0.0 };

            exact = new FundamentalSolution(gamma1, gamma2, lambda, k, y);


            // if (problem != null)
            solution = solver.Solve(exact.Problem, 64);


            InitRenderer(solution);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            var arr = textBox1.Text.Split(',').Select(double.Parse).ToArray();

            var exactf = solver.Solve(exact.Problem, 256).Val(arr);// exact.Val(arr);

            var n = new[] { 2,4, 8, 16, 32, 64, 128 };

            foreach (var v in n)
            {
                var p = solver.Solve(exact.Problem, v);
                var err = Abs(p.Val(arr) - exactf);
                dataGridView1.Rows.Add(v, err.ToString("N7"));
            }
        }
    }
}
