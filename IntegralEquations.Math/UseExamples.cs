﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Math
{
   public static class UseExamples
    {
        public static void Test()
        {
            var scalarF = new SymbolicScalarFunction("x1 + x2", 2);
            var val1 = scalarF.Val(new[] { 1.0, 4.0 });

            var scalarFt = new SymbolicScalarFunction("Cos(t)/t", 1, "t");
            var val2 = scalarFt.Val(new[] { 1.0 });

            IVectorFunction vec = new ParametricFunction(new SymbolicScalarFunction("Sin(t)", 1, "t"), new SymbolicScalarFunction("Cos(t)", 1, "t"));
            var vecVal = vec.Val(new[] { 1.0 });

            var handle = new ScalarFunctionHandle(x => System.Math.Sin(x[0]) / x[1], 2);
            var handleVal = handle.Val(new[] { 1.0, 1.0 });
        }

    }
}
