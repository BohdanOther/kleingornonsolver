﻿using System.Linq;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Math
{
    public static class FunctionHelpers
    {
        public static IScalarFunction Derivative(this IScalarFunction f, int parameterIndex = 0)
        {
            var deriv = MathNet.Numerics.Differentiate.FirstPartialDerivativeFunc(f.Val, parameterIndex);
            return new ScalarFunctionHandle(deriv, f.Dimention);
        }

        public static double[] VectorDifference(double[] x, double[] y)
        {
            return x.Zip(y, (f, s) => f - s).ToArray();
        }

        public static double ScalarProduct(double[] x, double[] y)
        {
            return x.Zip(y, (f, s) => f * s).Sum();
        }

        public static double EuclideanNorm(double[] v)
        {
            var res = System.Math.Sqrt(v.Sum(x => x * x));
            return res;
        }

        public static double EuclidianDistance(double[] x, double[] y)
        {
            return EuclideanNorm(VectorDifference(x, y));
        }

        public static bool NearlyEquals(double x, double y)
        {
            return System.Math.Abs(x - y) < Constants.Eps;
        }

        /// <summary>
        /// Wraps single double in vector array
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static double[] ToVectorArg(this double arg)
        {
            return new[] { arg };
        }
    }
}
