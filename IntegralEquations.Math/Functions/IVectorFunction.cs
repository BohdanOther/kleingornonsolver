﻿namespace IntegralEquations.Math.Functions
{
    /// <summary>
    /// f: Rn -> Rn
    /// takes vector, returns vector
    /// </summary>
    public interface IVectorFunction : IFunction
    {
        double[] Val(double[] args);
    }
}
