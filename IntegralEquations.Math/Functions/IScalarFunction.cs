﻿using System;

namespace IntegralEquations.Math.Functions
{
    /// <summary>
    /// f: Rn -> R
    /// takes vector, returns scalar
    /// </summary>
    public interface IScalarFunction : IFunction
    {
        double Val(double[] args);
    }
}
