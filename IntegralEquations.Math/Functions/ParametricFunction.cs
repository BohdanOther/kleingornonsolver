﻿namespace IntegralEquations.Math.Functions
{
    public class ParametricFunction : IVectorFunction
    {
        public int Dimention => 2;

        public IScalarFunction F1 => funcs[0];
        public IScalarFunction F2 => funcs[1];

        private readonly IScalarFunction[] funcs;

        /// <summary>
        /// Parametric function f(t) = { f1(t), f2(t)} }
        /// </summary>
        public ParametricFunction(IScalarFunction f1, IScalarFunction f2)
        {
            funcs = new[] { f1, f2 };
        }

        public double[] Val(double[] args)
        {
            var res = new[] { F1.Val(args), F2.Val(args) };

            return res;
        }
    }
}
