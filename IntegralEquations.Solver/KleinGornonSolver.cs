﻿using IntegralEquations.Solver.Core;
using MathNet.Numerics.LinearAlgebra;
using static IntegralEquations.Math.FunctionHelpers;
using static System.Math;

namespace IntegralEquations.Solver
{
    /// <summary>
    /// Solver of Klein-Gordon integral equasion
    /// Δu - k^2 * u = 0
    /// </summary>
    public class KleinGornonSolver
    {
        int M;
        KleinGordonProblem problem;

        public KleinGordonSolution Solve(KleinGordonProblem problem, int m)
        {
            M = m;
            this.problem = problem;

            // equidistant mesh
            var mesh = Vector<double>.Build.Dense(2 * M, j => PI * j / M).ToArray();

            var psi = SolvePsi(mesh);

            var kernel = KernelFactory.BuildContiniousKernel(this.problem, mesh);

            return new KleinGordonSolution(kernel, psi[0], psi[1], mesh);
        }

        private double[][] SolvePsi(double[] mesh)
        {
            // precalculate H-kernels
            var kernel = KernelFactory.BuildDiskreteKernel(problem, mesh);

            // fill matrixes
            var b = Vector<double>.Build.Dense(4 * M);
            var A = Matrix<double>.Build.Dense(4 * M, 4 * M);

            for (var i = 0; i < 2 * M; i++)
            {
                for (var j = 0; j < 2 * M; j++)
                {
                    A[i, j] = kernel.H11(i, j) / (2 * M);
                    A[i, j + 2 * M] = kernel.H12(i, j) / (2 * M);
                    A[i + 2 * M, j] = kernel.H21(i, j) / (2 * M);
                    A[i + 2 * M, j + 2 * M] = kernel.H22(i, j) / (2 * M);
                }

                var x1Dt = problem.OuterBound.Derivative.Val(mesh[i].ToVectorArg());
                var x2Dt = problem.InnerBound.Derivative.Val(mesh[i].ToVectorArg());

                A[i, i] += 0.5 / EuclideanNorm(x1Dt);
                A[i + 2 * M, i + 2 * M] += 0.5 / EuclideanNorm(x2Dt);

                b[i] = problem.GetF1(mesh[i]);
                b[i + 2 * M] = problem.GetF2(mesh[i]);
            }

            // solve system
            // var psi = A.SolveIterative(b, new TFQMR());
            var psi = A.Solve(b);
            var psi1 = psi.SubVector(0, 2 * M);
            var psi2 = psi.SubVector(2 * M, 2 * M);

            return new[] { psi1.ToArray(), psi2.ToArray() };
        }
    }
}