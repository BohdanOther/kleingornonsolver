﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Solver.FundamentalSolution
{
    public class FundamentalProblem : KleinGordonProblem
    {
        private readonly double[] y;

        /// <summary>
        /// Point outside region D
        /// </summary>
        /// <param name="y"></param>
        public FundamentalProblem(double[] y)
        {
            this.y = y;
        }

        public override double GetF1(double t)
        {
            var x = OuterBound.F.Val(t.ToVectorArg());

            var r = FunctionHelpers.EuclidianDistance(x, y);
            var xMinisY = FunctionHelpers.VectorDifference(x, y);
            var normal = OuterBound.Normal(t);

            // dPhi/dNu(x)
            return (-Kappa * SpecialFunctions.BesselK1(Kappa * r) * (FunctionHelpers.ScalarProduct(xMinisY, normal) / r)) / Constants.Pi2;
        }

        public override double GetF2(double t)
        {
            var arg = InnerBound.F.Val(t.ToVectorArg());
            return SpecialFunctions.Phi(arg, y, Kappa);
        }
    }
}
