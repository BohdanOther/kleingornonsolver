﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using IntegralEquations.Solver.Core;

namespace IntegralEquations.Solver.FundamentalSolution
{
    /// <summary>
    /// Exact solution for special case
    /// when u(x) = Φ(x, y*),
    /// where y* is fixed point outside region D
    /// </summary>
    public class FundamentalSolution : IScalarFunction
    {
        public int Dimention => 2;

        private readonly double[] y;
        public KleinGordonProblem Problem { get; }

        public FundamentalSolution(ParametricBoundary outerBound, ParametricBoundary innerBound, IScalarFunction lambda, double kappa, double[] y)
        {
            this.y = y;
            Problem = new FundamentalProblem(y)
            {
                Kappa = kappa,
                Lambda = lambda,
                OuterBound = outerBound,
                InnerBound = innerBound
            };
        }

        public double Val(double[] args)
        {
           // if (args[0] * args[0] + args[1] * args[1] < 0.25) return 0.0;
            return SpecialFunctions.Phi(args, y, Problem.Kappa);
        }

        public double Val(double x)
        {
            return Val(new[] { x, 0 });
        }


    }
}
