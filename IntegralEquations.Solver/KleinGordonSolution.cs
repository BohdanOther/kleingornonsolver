﻿using IntegralEquations.Math.Functions;
using IntegralEquations.Solver.Core;
using System;

namespace IntegralEquations.Solver
{
    public class KleinGordonSolution : IScalarFunction
    {
        public int Dimention => 2;

        private readonly int M;
        private readonly double[] mesh;
        private readonly double[] psi1;
        private readonly double[] psi2;
        private readonly ContiniousKernel kernel;

        public KleinGordonSolution(ContiniousKernel kernel, double[] psi1, double[] psi2, double[] mesh)
        {
            if (psi1.Length != psi2.Length)
                throw new Exception("Sizes of psi vecors are different");
            if (psi1.Length != mesh.Length)
                throw new Exception("Wrong mesh size");

            M = mesh.Length / 2;
            this.kernel = kernel;
            this.psi1 = psi1;
            this.psi2 = psi2;
            this.mesh = mesh;
        }

        private double GetVal(double[] x)
        {
            var val = 0.0;
            for (var i = 0; i < 2 * M; i++)
            {
                val += psi1[i] * kernel.Q1(x, mesh[i]) + psi2[i] * kernel.Q2(x, mesh[i]);
            }


            if (x[0]*x[0] + x[1]*x[1] < 0.25) return 0.14712586467430186;//0.0;
            return val / (2 * M);
        }

        public double Val(double[] args)
        {
            return GetVal(args);
        }

        public double Val(double x)
        {
            return Val(new[] { x, x });
        }
    }
}
