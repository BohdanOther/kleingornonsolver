﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using IntegralEquations.Solver.Core;

namespace IntegralEquations.Solver
{
    public class KleinGordonProblem
    {
        public double Kappa { get; set; }

        public IScalarFunction Lambda { get; set; }
        public IScalarFunction F1 { get; set; }
        public IScalarFunction F2 { get; set; }

        public ParametricBoundary OuterBound { get; set; }
        public ParametricBoundary InnerBound { get; set; }

        /// <summary>
        /// Calculates F1 = F1(x11(t), x12(t))
        /// where x1 is outer bound
        /// </summary>
        /// <param name="t">Value on outer bount</param>
        /// <returns></returns>
        public virtual double GetF1(double t)
        {
            var arg = OuterBound.F.Val(t.ToVectorArg());
            return F1.Val(arg);
        }

        /// <summary>
        /// Calculates F2 = F2(x21(t), x22(t))
        /// where x2 is inner bound
        /// </summary>
        /// <param name="t">Parameter on inner bount</param>
        /// <returns></returns>
        public virtual double GetF2(double t)
        {
            var arg = InnerBound.F.Val(t.ToVectorArg());
            return F2.Val(arg); 
        }
    }
}
