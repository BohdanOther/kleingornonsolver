﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Solver.Core
{
    public class ParametricBoundary
    {
        public ParametricFunction F { get; private set; }
        public ParametricFunction Derivative { get; private set; }

        public ParametricBoundary(ParametricFunction f, ParametricFunction df = null)
        {
            F = f;
            Derivative = df ?? CreateDerivFunctionHandle(f);
        }

        /// <summary>
        /// Outer normalized normal
        /// </summary>
        public double[] Normal(double t)
        {
            var norm = FunctionHelpers.EuclideanNorm(Derivative.Val(t.ToVectorArg()));
            var args = new[] { t };

            var res = new[]
            {
                Derivative.F2.Val(args)/norm,
                -Derivative.F1.Val(args)/ norm
            };

            return res;
        }

        private ParametricFunction CreateDerivFunctionHandle(ParametricFunction f)
        {
            return new ParametricFunction(f.F1.Derivative(), f.F2.Derivative());
        }
    }
}
