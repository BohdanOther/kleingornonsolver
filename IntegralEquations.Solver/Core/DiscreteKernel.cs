﻿using System;

namespace IntegralEquations.Solver.Core
{
    public class DiscreteKernel
    {
        private readonly int M;
        private readonly double[,] H;

        public DiscreteKernel(double[,] kernel)
        {
            M = kernel.GetLength(0) / 2;
            H = kernel;
        }

        public double H11(int i, int j) => H[i, j];
        public double H12(int i, int j) => H[i, j + M];
        public double H21(int i, int j) => H[i + M, j];
        public double H22(int i, int j) => H[i + M, j + M];
    }
}
