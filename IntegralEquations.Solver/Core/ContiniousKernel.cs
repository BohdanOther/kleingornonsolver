﻿using static IntegralEquations.Math.FunctionHelpers;
using static IntegralEquations.Math.SpecialFunctions;

namespace IntegralEquations.Solver.Core
{
    public class ContiniousKernel
    {
        private readonly KleinGordonProblem problem;

        public ContiniousKernel(KleinGordonProblem problem)
        {
            this.problem = problem;
        }

        public double Q1(double[] x, double tj)
        {
            var k = problem.Kappa;
            var y = problem.OuterBound.F.Val(tj.ToVectorArg());
            var r = EuclidianDistance(x, y);

            var kernel = BesselK0(k * r);

            return kernel;
        }

        public double Q2(double[] x, double tj)
        {
            var k = problem.Kappa;
            var y = problem.InnerBound.F.Val(tj.ToVectorArg());
            var r = EuclidianDistance(x, y);
            var xMinusY = VectorDifference(x, y);
            var normal = problem.InnerBound.Normal(tj);

            var kernel = k * BesselK1(k * r) * ScalarProduct(xMinusY, normal) / r;

            return kernel;
        }
    }
}
