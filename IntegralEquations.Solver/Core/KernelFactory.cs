﻿using IntegralEquations.Math;
using static IntegralEquations.Math.FunctionHelpers;
using static IntegralEquations.Math.SpecialFunctions;

namespace IntegralEquations.Solver.Core
{
    public static class KernelFactory
    {
        private static KleinGordonProblem _problem;
        private static int M;

        public static ContiniousKernel BuildContiniousKernel(KleinGordonProblem problem, double[] mesh)
        {
            return new ContiniousKernel(_problem);
        }

        public static DiscreteKernel BuildDiskreteKernel(KleinGordonProblem problem, double[] mesh)
        {
            _problem = problem;
            M = mesh.Length;
            var kernel = new double[2 * M, 2 * M];

            // precalculate discrete kernel values
            for (var i = 0; i < M; i++)
            {
                for (var j = 0; j < M; j++)
                {
                    kernel[i, j] = H11(mesh[i], mesh[j]);
                    kernel[i, j + M] = H12(mesh[i], mesh[j]);
                    kernel[i + M, j] = H21(mesh[i], mesh[j]);
                    kernel[i + M, j + M] = H22(mesh[i], mesh[j]);
                }
            }

            return new DiscreteKernel(kernel);
        }

        private static double H11(double t, double tau)
        {
            var k = _problem.Kappa;
            var x = _problem.OuterBound.F.Val(t.ToVectorArg());
            var y = _problem.OuterBound.F.Val(tau.ToVectorArg());
            var xMinusY = VectorDifference(x, y);
            var r = EuclidianDistance(x, y);
            var normal = _problem.OuterBound.Normal(t);
            var lambda = _problem.Lambda.Val(x);

            if (NearlyEquals(t, tau))
            {
                var dx11 = _problem.OuterBound.Derivative.F1.Val(t.ToVectorArg());
                var dx12 = _problem.OuterBound.Derivative.F2.Val(t.ToVectorArg());

                var ddx11 = _problem.OuterBound.Derivative.F1.Derivative().Val(t.ToVectorArg());
                var ddx12 = _problem.OuterBound.Derivative.F2.Derivative().Val(t.ToVectorArg());

                var normOfDx = EuclideanNorm(_problem.OuterBound.Derivative.Val(t.ToVectorArg()));

                var singularity1 = (dx12 * ddx11 - dx11 * ddx12) / (2 * System.Math.Pow(normOfDx, 3));
                var singularity2 = 0.5 * System.Math.Log(1.0 / (k * k * normOfDx * normOfDx)) + Sigma0(0) - 0.5 * BesselI0(k * r) * (2.0*R(t, tau, M) + 1.0);

                var giperSingularity = 1.0 + 0.5 * (2.0 * R(t, tau, M) + 1.0) * BesselI1(k * r) * r;

                return singularity1 * giperSingularity + lambda * singularity2;
            }

            // else no singularity
            var kernel = lambda * BesselK0(k * r) - k * BesselK1(k * r) * ScalarProduct(xMinusY, normal) / r;

            return kernel;
        }
        private static double H12(double t, double tau)
        {
            var k = _problem.Kappa;
            var x = _problem.OuterBound.F.Val(t.ToVectorArg());
            var y = _problem.InnerBound.F.Val(tau.ToVectorArg());
            var xMinusY = VectorDifference(x, y);
            var r = EuclidianDistance(x, y);
            var lambda = _problem.Lambda.Val(x);
            var normalX = _problem.OuterBound.Normal(t);
            var normalY = _problem.InnerBound.Normal(tau);

            var kernel = lambda * k * BesselK1(k * r) * ScalarProduct(xMinusY, normalY) / r
                - k * k * BesselKn(k * r, 2) * (ScalarProduct(xMinusY, normalX) * ScalarProduct(xMinusY, normalY) / (r * r));

            return kernel;
        }
        private static double H21(double t, double tau)
        {
            var k = _problem.Kappa;
            var x = _problem.InnerBound.F.Val(t.ToVectorArg());
            var y = _problem.OuterBound.F.Val(tau.ToVectorArg());
            var r = EuclidianDistance(x, y);

            var kernel = BesselK0(k * r);

            return kernel;
        }
        private static double H22(double t, double tau)
        {
            var k = _problem.Kappa;
            var x = _problem.InnerBound.F.Val(t.ToVectorArg());
            var y = _problem.InnerBound.F.Val(tau.ToVectorArg());
            var xMinusY = VectorDifference(x, y);
            var r = EuclidianDistance(x, y);
            var normal = _problem.InnerBound.Normal(tau);

            if (NearlyEquals(t, tau))
            {
                var dx21 = _problem.InnerBound.Derivative.F1.Val(t.ToVectorArg());
                var dx22 = _problem.InnerBound.Derivative.F2.Val(t.ToVectorArg());

                var ddx21 = _problem.InnerBound.Derivative.F1.Derivative().Val(t.ToVectorArg());
                var ddx22 = _problem.InnerBound.Derivative.F2.Derivative().Val(t.ToVectorArg());

                var normOfDx = EuclideanNorm(_problem.InnerBound.Derivative.Val(t.ToVectorArg()));

                var singularity = (dx22 * ddx21 - dx21 * ddx22) / (2 * System.Math.Pow(normOfDx, 3));

                var giperSingularity = 1.0 + 0.5 * (2.0 * R(t, tau, M) + 1.0) * BesselI1(k * r) * r;

                return singularity * giperSingularity;
            }

            // else no singularity
            var kernel = k * BesselK1(k * r) * ScalarProduct(xMinusY, normal) / r;

            return kernel;
        }
    }
}
