﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegralEquations.MathTests.Functions
{
    [TestClass]
    public class FunctionHelpersTests
    {
        [TestMethod]
        public void VectorDifferenceTest()
        {
            // arrange
            var x = new double[] { 1, 6, 1 };
            var y = new double[] { 1, 6, 1 };

            // act
            var actualVector = FunctionHelpers.VectorDifference(x, y);
            var expectedVector = new double[] { 0, 0, 0 };

            var actual = true;
            for (var i = 0; i < actualVector.Length; i++)
            {
                actual = actualVector[i].Equals(expectedVector[i]);
            }
            var expected = true;

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ScalarProductTest()
        {
            // arrange
            var x = new double[] { 1, 6, 1 };
            var y = new double[] { 1, 6, 1 };

            // act
            var actual = FunctionHelpers.ScalarProduct(x, y);
            var expected = 38.0;

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }

        [TestMethod]
        public void EuclideanNormTest()
        {
            // arrange
            var x = new double[] { 3, 4, 0 };

            // act
            var actual = FunctionHelpers.EuclideanNorm(x);
            var expected = 5.0;

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }

        [TestMethod]
        public void EuclidianDistanceTest()
        {
            // arrange
            var x = new double[] { 1, 6, 1 };
            var y = new double[] { 1, 6, 1 };

            // act
            var actual = FunctionHelpers.EuclidianDistance(x, y);
            var expected = 0.0;

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }

        [TestMethod]
        public void EqualsTest()
        {
            // arrange
            var x = 6.6;
            var y = 6.6;

            // act
            var actual = Equals(x, y);
            var expected = true;

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DerivativeTest()
        {
            // arrange
            var f = new ScalarFunctionHandle((x) => x[0] * x[0] - 7 * x[0], 1); // x^2 - 7x           
            var df = f.Derivative();  // 2*x - 7
            var arg = new double[] { 1.0 };

            // act
            var actual = df.Val(arg);
            var expected = 2.0 - 7.0;

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);

        }
    }
}