﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using static IntegralEquations.Math.SpecialFunctions;

namespace IntegralEquations.Math.Functions.Tests
{
    [TestClass()]
    public class SpecialFunctionsTests
    {
        [TestMethod()]
        public void BesselKnTest_n0()
        {
            // arrange
            var z = 3.0;
            var n = 0;

            // act
            var actual = BesselKn(z, n);
            var expected = BesselK0(z);

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }

        [TestMethod()]
        public void BesselKnTest_n1()
        {
            // arrange
            var z = 3.0;
            var n = 1;

            // act
            var actual = BesselKn(z, n);
            var expected = BesselK1(z);

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }

        [TestMethod()]
        public void BesselKnTest_n2()
        {
            // arrange
            var z = 3.0;
            var n = 2;

            // act
            var actual = BesselKn(z, n);
            var expected = 0.0615104586;

            // assert
            Assert.AreEqual(expected, actual, Constants.Eps);
        }
    }
}